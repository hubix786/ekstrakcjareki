/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//package projektzespolowy;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 *
 * @author IBM
 */
public class Main extends javax.swing.JFrame {

    BufferedImage defaultImage, convertedImage;
    float tablica[][] = new float[20][3];
        int cnt = 0;
    /**
     * Creates new form Main
     */
    static int[][] pixelMatrix;

    public Main() {
        initComponents();
        this.setTitle("HandExtraction");
        defaultImageLabel.addMouseListener(adapter);

    }

    MouseAdapter adapter = new MouseAdapter() {
/*
        @Override
        public void mouseClicked(MouseEvent e) {
            super.mouseClicked(e); //To change body of generated methods, choose Tools | Templates.
            try {

                Set<Integer> colorsList = new HashSet<>();
                Robot robot = new Robot();
                Color c = robot.getPixelColor(e.getXOnScreen(), e.getYOnScreen());
                int selectedRed = c.getRed();
                int selectedGreen = c.getGreen();
                int selectedBlue = c.getBlue();

                int lowestRed = (int)(selectedRed / 2);
                int lowestGreen =(int)(selectedGreen / 2);
                int lowestBlue =(int)(selectedBlue / 2);

                int highestRed = (int) (selectedRed * 1.05);
                int highestGreen = (int) (selectedGreen * 1.05);
                int highestBlue = (int) (selectedBlue * 1.05);

                JOptionPane.showMessageDialog(rootPane, "Founded color: " + selectedRed + " " + selectedGreen + " " + selectedBlue);

                //tworzenie listy kolorów podobnych do wybranego
                while (lowestRed < highestRed || lowestGreen < highestGreen || lowestBlue < highestBlue) {
                    colorsList.add(new Color(lowestRed, lowestGreen, lowestBlue).getRGB());
                    for (int i = 0; i < 100; i++) {
                        int higherRed = (lowestRed+i>255)?255:(lowestRed+i);
                        int higherGreen = (lowestGreen+i>255)?255:(lowestGreen+i);
                        int higherBlue = (lowestBlue+i>255)?255:(lowestBlue+i);
                        int lowerRed = (lowestRed-i<0)?0:(lowestRed-i);
                        int lowerGreen = (lowestGreen-i<0)?0:(lowestGreen-i);
                        int lowerBlue = (lowestBlue-i<0)?0:(lowestBlue-i);
                        colorsList.add(new Color(higherRed, lowestGreen, lowestBlue).getRGB());
                        colorsList.add(new Color(lowestRed, higherGreen, lowestBlue).getRGB());
                        colorsList.add(new Color(lowestRed, lowestGreen, higherBlue).getRGB());
                        colorsList.add(new Color(lowerRed, lowestGreen, lowestBlue).getRGB());
                        colorsList.add(new Color(lowestRed, lowerGreen, lowestBlue).getRGB());
                        colorsList.add(new Color(lowestRed, lowestGreen, lowerBlue).getRGB());
                        colorsList.add(new Color(higherRed, higherGreen, lowestBlue).getRGB());
                        colorsList.add(new Color(lowestRed, higherGreen, higherBlue).getRGB());
                        colorsList.add(new Color(higherRed, lowestGreen, higherBlue).getRGB());
                        colorsList.add(new Color(lowerRed, lowerGreen, lowestBlue).getRGB());
                        colorsList.add(new Color(lowestRed, lowerGreen, lowerBlue).getRGB());
                        colorsList.add(new Color(lowerRed, lowestGreen, lowerBlue).getRGB());
                        colorsList.add(new Color(higherRed, lowerGreen, lowestBlue).getRGB());
                        colorsList.add(new Color(lowerRed, higherGreen, lowestBlue).getRGB());
                        colorsList.add(new Color(lowestRed, higherGreen, lowerBlue).getRGB());
                        colorsList.add(new Color(lowestRed, lowerGreen, higherBlue).getRGB());
                        colorsList.add(new Color(higherRed, higherGreen, lowerBlue).getRGB());
                        colorsList.add(new Color(lowerRed, higherGreen, higherBlue).getRGB());
                        colorsList.add(new Color(higherRed, lowerGreen, higherBlue).getRGB());
                        colorsList.add(new Color(higherRed, lowerGreen, lowerBlue).getRGB());
                        colorsList.add(new Color(lowerRed, higherGreen, lowerBlue).getRGB());
                        colorsList.add(new Color(lowerRed, lowerGreen, higherBlue).getRGB());
                        colorsList.add(new Color(higherRed, higherGreen, higherBlue).getRGB());
                        colorsList.add(new Color(lowerRed, lowerGreen, lowerBlue).getRGB());
                    }

                    if (lowestRed < highestRed) {
                        lowestRed++;
                    }
                    if (lowestGreen < highestGreen) {
                        lowestGreen++;
                    }
                    if (lowestBlue < highestBlue) {
                        lowestBlue++;
                    }
                }

                JOptionPane.showMessageDialog(rootPane, "color list created");

                int blackColor = new Color(0, 0, 0).getRGB();
                int imageWidth = defaultImage.getWidth();
                int imageHeight = defaultImage.getHeight();
                int[] pixelArray = new int[imageHeight * imageWidth];
                defaultImage.getRGB(0, 0, imageWidth, imageHeight, pixelArray, 0, imageWidth);
                for (int i = 0; i < pixelArray.length; i++) {
                    if (colorsList.contains(pixelArray[i])) {
                    } else {
                        pixelArray[i] = blackColor;
                    }
                }
                convertedImage.setRGB(0, 0, imageWidth, imageHeight, pixelArray, 0, imageWidth);
                Rectangle r = defaultImageLabel.getVisibleRect();
                editedImgLabel.setIcon(new ImageIcon(convertedImage.getScaledInstance(r.width, r.height, Image.SCALE_SMOOTH)));
                JOptionPane.showMessageDialog(rootPane, "Processing image completed");

            } catch (AWTException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }*/
        
        @Override
        public void mouseClicked(MouseEvent e) {
            super.mouseClicked(e); //To change body of generated methods, choose Tools | Templates.
            try {

                Set<Integer> colorsList = new HashSet<>();
                Robot robot = new Robot();
                Color c = robot.getPixelColor(e.getXOnScreen(), e.getYOnScreen());
                int selectedRed = c.getRed();
                int selectedGreen = c.getGreen();
                int selectedBlue = c.getBlue();

                int red, green, blue;
                float colors[] = new float[3];   
                
                Color.RGBtoHSB(selectedRed,selectedGreen,selectedBlue,colors);                
                
                JOptionPane.showMessageDialog(rootPane, "Founded color: " + colors[0] + " " + colors[1] + " " 
                        + colors[2] /*+ "\n\nPozostało " + l + " próbek do zebrania."*/);


                int blackColor = new Color(0, 0, 0).getRGB();
                int imageWidth = defaultImage.getWidth();
                int imageHeight = defaultImage.getHeight();
                int[] pixelArray = new int[imageHeight * imageWidth];
                defaultImage.getRGB(0, 0, imageWidth, imageHeight, pixelArray, 0, imageWidth);
                
                
                //biorę każdy piksel z obrazka, pobieram jego wartości RGB, konwertuję na HSB i porównuję z przyjętymi ekperymentalnie przedziałami
                //każdy piksel nie pasujący do wzorca zostaje zamieniony na kolor czarny
                for (int i = 0; i < pixelArray.length; i++) {
                    Color co = new Color(pixelArray[i]);
                    Color.RGBtoHSB(co.getRed(),co.getGreen(),co.getBlue(),colors);
                    if(!(((colors[0] >= 0.53) && (colors[0] <=0.63))
                        && ((colors[1] >= 0.1) && (colors[1] <=0.7))
                        && ((colors[2] >= 0.1) && (colors[2] <=0.9)))){
                            pixelArray[i] = blackColor;
                    }
                }
                
                convertedImage.setRGB(0, 0, imageWidth, imageHeight, pixelArray, 0, imageWidth);//skala szarości na podstawie tablicy pikseli
                Rectangle r = defaultImageLabel.getVisibleRect();
                editedImgLabel.setIcon(new ImageIcon(convertedImage.getScaledInstance(r.width, r.height, Image.SCALE_SMOOTH)));
                JOptionPane.showMessageDialog(rootPane, "Processing image completed");

            } catch (AWTException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    };

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane = new javax.swing.JTabbedPane();
        originalImgPanel = new javax.swing.JPanel();
        defaultImageLabel = new javax.swing.JLabel();
        editedImgPanel = new javax.swing.JPanel();
        editedImgLabel = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        loadImage = new javax.swing.JMenuItem();
        HelpMenu = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout originalImgPanelLayout = new javax.swing.GroupLayout(originalImgPanel);
        originalImgPanel.setLayout(originalImgPanelLayout);
        originalImgPanelLayout.setHorizontalGroup(
            originalImgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(defaultImageLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 920, Short.MAX_VALUE)
        );
        originalImgPanelLayout.setVerticalGroup(
            originalImgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(defaultImageLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 469, Short.MAX_VALUE)
        );

        jTabbedPane.addTab("Original", originalImgPanel);

        javax.swing.GroupLayout editedImgPanelLayout = new javax.swing.GroupLayout(editedImgPanel);
        editedImgPanel.setLayout(editedImgPanelLayout);
        editedImgPanelLayout.setHorizontalGroup(
            editedImgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(editedImgLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 920, Short.MAX_VALUE)
        );
        editedImgPanelLayout.setVerticalGroup(
            editedImgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(editedImgLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 469, Short.MAX_VALUE)
        );

        jTabbedPane.addTab("Grayscale", editedImgPanel);

        jMenu1.setText("File");

        loadImage.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        loadImage.setText("Load image");
        loadImage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadImageActionPerformed(evt);
            }
        });
        jMenu1.add(loadImage);

        jMenuBar1.add(jMenu1);

        HelpMenu.setText("Help");

        jMenuItem1.setText("About");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        HelpMenu.add(jMenuItem1);

        jMenuBar1.add(HelpMenu);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 496, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private static int[] rgb2hsv(int red, int grn, int blu){
        int hue, sat, val;
        int x, f, i;
        int result[] = new int[3];

        x = Math.min(Math.min(red, grn), blu);
        val = Math.max(Math.max(red, grn), blu);
        if (x == val){
            hue = 0;
            sat = 0;
        }
        else {
        f = (red == x) ? grn-blu : ((grn == x) ? blu-red : red-grn);
        i = (red == x) ? 3 : ((grn == x) ? 5 : 1);
        hue = ((i-f/(val-x))*60)%360;
        sat = ((val-x)/val);
        }
        result[0] = hue;
        result[1] = sat;
        result[2] = val;
        return result;
}
    
    
    private void loadImageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadImageActionPerformed
        JFileChooser fileChooser = new JFileChooser();
        if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            try {
                defaultImage = ImageIO.read(fileChooser.getSelectedFile());
                pixelMatrix = getPixelMatrix(defaultImage);
                convertedImage = defaultImage;//getGrayscaleByPixels(defaultImage);\
                Rectangle r = defaultImageLabel.getVisibleRect();
                defaultImageLabel.setIcon(new ImageIcon(defaultImage.getScaledInstance(r.width, r.height, Image.SCALE_SMOOTH)));
                editedImgLabel.setIcon(new ImageIcon(convertedImage.getScaledInstance(r.width, r.height, Image.SCALE_SMOOTH)));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }//GEN-LAST:event_loadImageActionPerformed


    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        JOptionPane.showMessageDialog(rootPane, "HandExtraction\n\nFor educational purposes only for Team Project classes\n\nAuthors:\nPaweł Dróżdż\nMarcin Grądzki\nHubert Sochacki");
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private Rectangle scaleImage(BufferedImage image) {
        Rectangle size = defaultImageLabel.getVisibleRect();
        int original_width = image.getWidth();
        int original_height = image.getHeight();
        int bound_width = size.width;
        int bound_height = size.height;
        int new_width = original_width;
        int new_height = original_height;

        // first check if we need to scale width
        if (original_width > bound_width) {
            //scale width to fit
            new_width = bound_width;
            //scale height to maintain aspect ratio
            new_height = (new_width * original_height) / original_width;
        }

        // then check if we need to scale even with the new height
        if (new_height > bound_height) {
            //scale height to fit instead
            new_height = bound_height;
            //scale width to maintain aspect ratio
            new_width = (new_height * original_width) / original_height;
        }

        return new Rectangle(new_width, new_height);
    }

//    private BufferedImage getGrayscaleByPixels(BufferedImage image)
//    {
//        BufferedImage result = image;
//        int width = result.getWidth();
//        int height = result.getHeight();
//        pixelMatrix = getPixelMatrix(image);
//        for (int i=0;i<width;i++)
//        {
//            for (int j=0;j<height;j++)
//            {
//                Color c = new Color(result.getRGB(i, j));
//                int grayLevel = (c.getBlue()+c.getGreen()+c.getRed())/3;
//                Color grayscale = new Color(grayLevel, grayLevel, grayLevel, c.getAlpha());
//                result.setRGB(i, j, grayscale.getRGB());
//                pixelMatrix[i][j] = grayLevel;
//            }
//        }
//        showMatrix(pixelMatrix,width,height);
//        return result;
//    }
    private void showMatrix(float[][] matrix, int width, int height) {

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println("");
        }
        
        
    }

    private static int[][] getPixelMatrix(BufferedImage image) {

        final byte[] pixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
        final int width = image.getWidth();
        final int height = image.getHeight();
        final boolean hasAlphaChannel = image.getAlphaRaster() != null;

        int[][] result = new int[height][width];
        if (hasAlphaChannel) {
            final int pixelLength = 4;
            for (int pixel = 0, row = 0, col = 0; pixel < pixels.length; pixel += pixelLength) {
                int argb = 0;
                argb += (((int) pixels[pixel] & 0xff) << 24); // alpha
                argb += ((int) pixels[pixel + 1] & 0xff); // blue
                argb += (((int) pixels[pixel + 2] & 0xff) << 8); // green
                argb += (((int) pixels[pixel + 3] & 0xff) << 16); // red
                result[row][col] = argb;
                col++;
                if (col == width) {
                    col = 0;
                    row++;
                }
            }
        } else {
            final int pixelLength = 3;
            for (int pixel = 0, row = 0, col = 0; pixel < pixels.length; pixel += pixelLength) {
                int argb = 0;
                argb += -16777216; // 255 alpha
                argb += ((int) pixels[pixel] & 0xff); // blue
                argb += (((int) pixels[pixel + 1] & 0xff) << 8); // green
                argb += (((int) pixels[pixel + 2] & 0xff) << 16); // red
                result[row][col] = argb;
                col++;
                if (col == width) {
                    col = 0;
                    row++;
                }
            }
        }

        return result;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Main().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu HelpMenu;
    private javax.swing.JLabel defaultImageLabel;
    private javax.swing.JLabel editedImgLabel;
    private javax.swing.JPanel editedImgPanel;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JTabbedPane jTabbedPane;
    private javax.swing.JMenuItem loadImage;
    private javax.swing.JPanel originalImgPanel;
    // End of variables declaration//GEN-END:variables
}
